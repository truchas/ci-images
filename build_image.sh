#!/bin/bash

set -xe

function full_image_name ()
{
    image_base_name=$1
    if [[ $CI_COMMIT_TAG == "" ]]; then
        # The commit is not tagged, use the first 7 chars of git commit hash
        image_version=${CI_COMMIT_SHA:0:7}
        DNAME="$CI_REGISTRY_IMAGE/ci-test:$image_base_name-$image_version"
    else
        # The commit is tagged, convert tag version properly:v1.0.1 -> "1.0.1"
        image_version=${CI_COMMIT_TAG:1}
        DNAME="$CI_REGISTRY_IMAGE:$image_base_name-$image_version"
    fi
    echo $DNAME
}

image_base_name=$1  # Must be the name of the directory
DNAME=$(full_image_name $image_base_name)
container_name="temp-container-$image_base_name-$CI_PIPELINE_IID"

# For TPL images, compute the dependency "base" name, and update the Dockerfile
# Use @ for sed delimiter, since the DNAME may contane slashes.
base_image_base_name=$(sed 's/tpl/base/' <<< $image_base_name)
base_DNAME=$(full_image_name $base_image_base_name)
sed -i "s@__BASE_DOCKER_IMAGE__@$base_DNAME@g" $image_base_name/Dockerfile

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY

docker build -t "$DNAME" ./$image_base_name

docker run -v /home/truchas/intel:/opt/intel --name "$container_name" "$DNAME" ./build.sh
docker commit --change="CMD bash" "$container_name" "$DNAME"
docker rm "$container_name"

docker push "$DNAME"
