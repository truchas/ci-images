#!/bin/bash

set -ex

# Load g++ version 12
source /opt/rh/gcc-toolset-12/enable
g++ --version

# Prepend our `python` executable into path
export PATH="${HOME}/ext/python-install/bin:$PATH"

mkdir tmp
cd tmp
git clone https://gitlab.com/truchas/truchas-tpl.git
cd truchas-tpl
git checkout v24
cmake -S. -Bbuild \
      -C config/linux-gcc.cmake \
      -G Ninja \
      -DCMAKE_INSTALL_PREFIX=${HOME}/ext/ \
      -DSEARCH_FOR_HDF5=no \
      -DSEARCH_FOR_NETCDF=no \
      -DBUILD_PORTAGE=ON
cd build
VERBOSE=1 ninja
cd ../../..
rm -rf tmp
