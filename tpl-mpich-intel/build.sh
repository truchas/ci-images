#!/bin/bash

set -ex

# Prepend our `python` executable into path
export PATH="${HOME}/ext/python-install/bin:$PATH"

export INTELDIR=/opt/intel/oneapi/2024.0
export PATH=$INTELDIR/bin/:$PATH
export LD_LIBRARY_PATH=$INTELDIR/lib
export FC=ifx
export CC=icx
export CXX=icpx

mkdir tmp
cd tmp
git clone https://gitlab.com/truchas/truchas-tpl.git
cd truchas-tpl
git checkout v24
cmake -S. -Bbuild \
      -C config/linux-intel.cmake \
      -G Ninja \
      -DCMAKE_INSTALL_PREFIX=${HOME}/ext/ \
      -DSEARCH_FOR_HDF5=no \
      -DSEARCH_FOR_NETCDF=no \
      -DBUILD_PORTAGE=ON
cd build
VERBOSE=1 ninja
cd ../../..
rm -rf tmp
