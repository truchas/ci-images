#!/bin/bash

set -ex

# Install CMake
wget -qO- https://github.com/Kitware/CMake/releases/download/v3.22.2/cmake-3.22.2-linux-x86_64.tar.gz | sudo tar xz --strip=1 -C /usr/local/
which cmake
cmake --version

# Install Python, numpy, h5py
mkdir tmp
cd tmp
git clone --depth=1 https://github.com/python-cmake-buildsystem/python-cmake-buildsystem.git
mkdir python-build
cd python-build
cmake -DCMAKE_INSTALL_PREFIX:PATH=${HOME}/ext/python-install ../python-cmake-buildsystem
make -j8
make install
cd ..
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
${HOME}/ext/python-install/bin/python get-pip.py
${HOME}/ext/python-install/bin/python -m pip install numpy h5py
export PATH="${HOME}/ext/python-install/bin:$PATH"

# Load g++ version 12
source /opt/rh/gcc-toolset-12/enable
g++ --version

# Build MPICH
wget https://www.mpich.org/static/downloads/4.2.1/mpich-4.2.1.tar.gz
tar xaf mpich-4.2.1.tar.gz
cd mpich-4.2.1
FFLAGS=-fallow-argument-mismatch ./configure --prefix=$HOME/ext --with-device=ch4:ofi
make -j8 all
make install
cd ../..
rm -rf tmp
