#!/bin/bash

set -ex

# Install CMake
wget -qO- https://github.com/Kitware/CMake/releases/download/v3.22.2/cmake-3.22.2-linux-x86_64.tar.gz | sudo tar xz --strip=1 -C /usr/local/
which cmake
cmake --version

# Install Python, numpy, h5py
mkdir tmp
cd tmp
git clone --depth=1 https://github.com/python-cmake-buildsystem/python-cmake-buildsystem.git
mkdir python-build
cd python-build
cmake -DCMAKE_INSTALL_PREFIX:PATH=${HOME}/ext/python-install ../python-cmake-buildsystem
make -j8
make install
cd ..
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
${HOME}/ext/python-install/bin/python get-pip.py
${HOME}/ext/python-install/bin/python -m pip install numpy h5py
export PATH="${HOME}/ext/python-install/bin:$PATH"

export INTELDIR=/opt/intel/oneapi/2024.0
export PATH=$INTELDIR/bin/:$PATH
export LD_LIBRARY_PATH=$INTELDIR/lib
export FC=ifx
export CC=icx
export CXX=icpx

# Build MPICH
# Failing due to this error: https://github.com/pmodels/mpich/issues/6978
# wget https://www.mpich.org/static/downloads/4.2.1/mpich-4.2.1.tar.gz
# tar xaf mpich-4.2.1.tar.gz
# cd mpich-4.2.1
### BUILDING MAIN ###
# Grabbing main now, upgrade to 4.2.2 when released. Then remove this block.
git clone --recursive https://github.com/pmodels/mpich
cd mpich
./autogen.sh
### BUILDING MAIN ###
./configure --prefix=$HOME/ext
make -j8 all
make install

cd ../..
rm -rf tmp

# # Build OpenMPI
# mkdir tmp
# cd tmp
# wget https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-4.1.6.tar.gz
# tar xaf openmpi-4.1.6.tar.gz
# cd openmpi-4.1.6
# ./configure --prefix=$HOME/ext
# make -j8 all
# make install
# cd ../..
# rm -rf tmp
