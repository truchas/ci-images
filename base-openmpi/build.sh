#!/bin/bash

set -ex

export INTELDIR=/opt/intel/19.1/
export PATH=$INTELDIR/bin/:$PATH
export LD_LIBRARY_PATH=$INTELDIR/lib/intel64
export FC=ifort
export CC=icc
export CXX=icpc

mkdir tmp
cd tmp

wget https://download.open-mpi.org/release/open-mpi/v4.0/openmpi-4.0.3.tar.bz2
tar xaf openmpi-4.0.3.tar.bz2
cd openmpi-4.0.3
./configure --prefix=$HOME/ext
make
make install
cd ../..

rm -rf tmp
